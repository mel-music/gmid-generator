const removeAccents = require("remove-accents");

function generalizeString(string) {
  if (!string) return string;
  string = removeAccents(string)
    .toLowerCase()
    .replace(/\$/g, "s")
    .replace(/€/g, "e")
    .replace(/¥/g, "y")
    .replace(/ /g, "");
  for (let i = 0; i < string.length; i++) {
    let charCode = string.charCodeAt(i);
    if (charCode >= 97 && charCode < 123) continue;
    charCode = (charCode % 26) + 97;
    string =
      string.substr(0, i) +
      String.fromCharCode(charCode) +
      string.substr(i + 1);
  }
  return string;
}

function generateArtistGmid(artistName) {
  return generalizeString(artistName);
}

function generateReleaseGmid(releaseTitle, artistName) {
  const artistGmid = generateArtistGmid(artistName);
  const releaseInfo = generalizeString(releaseTitle);
  return `${releaseInfo}:${artistGmid}`;
}

function generateTrackGmid(trackTitle, trackNumber, releaseTitle, artistName) {
  if (!artistName) {
    artistName = releaseTitle;
    releaseTitle = trackNumber;
  }

  const releaseGmid = generateReleaseGmid(releaseTitle, artistName);
  const trackInfo = generalizeString(trackTitle);

  if (trackNumber) {
    trackNumber = trackNumber + "";
    trackNumber = trackNumber.padStart(3, "0");
  } else {
    trackNumber = "000";
  }
  return `${trackNumber}${trackInfo}:${releaseGmid}`;
}

module.exports = {
  generalizeString,
  generateArtistGmid,
  generateReleaseGmid,
  generateTrackGmid,
};

